AC_INIT
AM_INIT_AUTOMAKE(faad2, 2.0)

AC_PROG_LIBTOOL
AC_SUBST(LIBTOOL_DEPS)

dnl Checks for programs.
AC_PROG_CXX
AC_PROG_CC
AC_PROG_CPP
AC_PROG_INSTALL
AC_PROG_LN_S
AC_PROG_MAKE_SET
AC_CHECK_PROGS(RPMBUILD, rpmbuild, rpm)

AM_CONFIG_HEADER(config.h)

AC_ARG_WITH( xmms,   [  --with-xmms             compile XMMS plugins],WITHXMMS=$withval, WITHXMMS=no)

AC_ARG_WITH( drm,    [  --with-drm              compile libfaad with DRM support],WITHDRM=$withval, WITHDRM=no)

dnl Checks for header files required for mp4.h
AC_HEADER_STDC
AC_CHECK_HEADERS(stdint.h inttypes.h)
AC_CHECK_HEADERS(mathf.h)
AC_CHECK_HEADERS(float.h)
AC_CHECK_FUNCS(strchr memcpy)
AC_CHECK_HEADERS(sys/time.h)
AC_HEADER_TIME

AC_C_INLINE
AC_C_BIGENDIAN

AC_TYPE_OFF_T

AC_DEFUN(MY_CHECK_TYPEDEF_FROM_INCLUDE,
[
   AC_MSG_CHECKING([for $1])
   AC_TRY_COMPILE([$2],
                  [$1;],
                  libfaad_ok=yes, libfaad_ok=no)
   if test $libfaad_ok = yes; then
      AC_DEFINE($3, 1, [Define if needed])
      AC_MSG_RESULT([yes])
   else
      AC_MSG_RESULT([no])
fi
])

dnl @synopsis AC_C99_FUNC_LRINTF
dnl
dnl Check whether C99's lrintf function is available.
dnl @version 1.3        Feb 12 2002
dnl @author Erik de Castro Lopo <erikd AT mega-nerd DOT com>
dnl
dnl Permission to use, copy, modify, distribute, and sell this file for any
dnl purpose is hereby granted without fee, provided that the above copyright
dnl and this permission notice appear in all copies.  No representations are
dnl made about the suitability of this software for any purpose.  It is
dnl provided "as is" without express or implied warranty.
dnl
AC_DEFUN([AC_C99_FUNC_LRINTF],
[AC_CACHE_CHECK(for lrintf,
  ac_cv_c99_lrintf,
[
lrintf_save_CFLAGS=$CFLAGS
CFLAGS="-O -lm"
AC_TRY_LINK([
#define         _ISOC9X_SOURCE  1
#define         _ISOC99_SOURCE  1
#define         __USE_ISOC99    1
#define         __USE_ISOC9X    1

#include <math.h>
], if (!lrintf(3.14159)) lrintf(2.7183);, ac_cv_c99_lrintf=yes, ac_cv_c99_lrintf=no)

CFLAGS=$lrintf_save_CFLAGS

])

if test "$ac_cv_c99_lrintf" = yes; then
  AC_DEFINE(HAVE_LRINTF, 1,
            [Define if you have C99's lrintf function.])
fi
])# AC_C99_FUNC_LRINTF
AC_C99_FUNC_LRINTF

MY_CHECK_TYPEDEF_FROM_INCLUDE([float32_t temp],
        [#include <sys/types.h>,
        #include <sys/float.h>], [HAVE_FLOAT32_T])

AC_CHECK_FUNCS(strsep)

MY_CHECK_TYPEDEF_FROM_INCLUDE([in_port_t temp],
        [#include <sys/types.h>
         #include <netinet/in.h>], [HAVE_IN_PORT_T])
MY_CHECK_TYPEDEF_FROM_INCLUDE([socklen_t temp],
        [#include <sys/types.h>
         #include <sys/socket.h>], HAVE_SOCKLEN_T)
MY_CHECK_TYPEDEF_FROM_INCLUDE([fpos_t foo; foo.__pos = 0;],
        [#include <stdio.h>],
        [HAVE_FPOS_T_POS])

AM_CONDITIONAL(WITH_MP4V2, false)
AM_CONDITIONAL(HAVE_MPEG4IP, false)

if test x$WITHXMMS = xyes; then
  AC_CHECK_PROGS(XMMS_CONFIG, xmms-config,"not_found")
  if test "$XMMS_CONFIG" = "not_found"; then
    AC_MSG_ERROR("*** xmms-config not found - xmms plugin can't be build")
  fi
  AC_CHECK_HEADER(pthread.h,,
           AC_MSG_ERROR(*** pthread headers support not installed or not found))
  AC_CHECK_HEADER(id3.h,,
       AC_MSG_ERROR(*** id3lib headers support not installed or not found))
  AC_CHECK_PROGS(GTK_CONFIG, gtk-config, "not_found")
  if test "$XGTK_CONFIG" = "not_found"; then
    AC_MSG_ERROR("*** gtk-config not found - xmms plugin can't be build without")
  fi

  AM_CONDITIONAL(HAVE_XMMS, true)
  AC_CONFIG_FILES(plugins/xmms/Makefile plugins/xmms/src/Makefile)
else
 AC_MSG_NOTICE(no xmms build configured)
 AM_CONDITIONAL(HAVE_XMMS, false)
fi

if test x$WITHDRM = xyes; then
  AC_DEFINE(DRM, 1, [Define if you want to use libfaad together with Digital Radio Mondiale (DRM)])
fi

AC_CONFIG_FILES(libfaad/Makefile frontend/Makefile common/Makefile plugins/Makefile Makefile )

AC_CONFIG_FILES(plugins/mpeg4ip/Makefile)

AC_CONFIG_FILES(common/mp4ff/Makefile common/mp4v2/Makefile)

AC_CONFIG_FILES(faad2.spec)
AC_OUTPUT
