The test files in this directory mostly have dimension 157x151.
The naming convention is

   photometric-channels-bits.tiff

minisblack-1c-16b.tiff
minisblack-1c-8b.tiff
miniswhite-1c-1b.tiff
palette-1c-1b.tiff
palette-1c-4b.tiff
palette-1c-8b.tiff
rgb-3c-16b.tiff
rgb-3c-8b.tiff

logluv-3c-16b.tiff: logluv compression/photometric interp
minisblack-2c-8b-alpha.tiff: grey+alpha




