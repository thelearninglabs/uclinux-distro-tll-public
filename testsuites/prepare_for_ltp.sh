#!/bin/sh
USER=/home/tll
UCLINUX_DIST_PATH=$USER/TLL/uClinux_distro/uclinux-dist
UCLINUX_DIST_SCRIPTS=$USER/test_scripts/uclinux-dist
mkdir -p $UCLINUX_DIST_SCRIPTS
for f in collect_performance.exp get_test_summary compare_kernel_results test_runtime_control get_benchmark_result     run_kernel_test   startup_test.sh get_performance_result   test_board_config
do
   cp $UCLINUX_DIST_PATH/testsuites/test_scripts/$f $UCLINUX_DIST_SCRIPTS/$f
   chmod +x $UCLINUX_DIST_SCRIPTS/$f
done



