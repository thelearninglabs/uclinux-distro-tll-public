#!/usr/bin/expect

source  ../kernel_config.exp
log_file [log_file_name "$argv0"]
send_user "Starting $argv0\n"
set TITLE [title "$argv0"]

step "Spawn kermit"
source ../spawn_kermit.exp

step "Reboot the kernel."
source ../reboot_kernel.exp 

proc usb_test {device mnt_point} {
    global case_num
    global kernel_prompt

    incr case_num
    set timeout 5
    send "mount -t vfat $device $mnt_point\r"
    while 1 {
        expect {
            -re "fail.*>" {
                send_user "mount $device failed\n"
                case_fail $case_num
            }
            -re $kernel_prompt {
                break
            }
            timeout {
                send -s "\3"
                send_user "mount $device timeout\n"
                break
            }
        }
    }

    send "mount\r"
    while 1 {
        expect {
            -re "$device on $mnt_point type vfat.*>" {
                send_user "mount $device succeeds\n"
                case_pass $case_num
                break
            }
            timeout {
                send_user "mount $device failed\n"
                case_fail $case_num
            }
        }
    }

    incr case_num
    send "echo teststring > $mnt_point/usbhost_testfile; cat $mnt_point/usbhost_testfile\r"
    while 1 {
        expect {
            -re "teststring\r\n$kernel_prompt" {
                case_pass $case_num
                break
            }
            timeout {
                send_user "Write on $mnt_point failed\n"
                case_fail $case_num
            }
        }
    }

    incr case_num
    set timeout 40
    send "time dd conv=fsync if=/dev/zero of=$mnt_point/10m.bin bs=1M count=10\r"
    while 1 {
        expect {
            -re "records in.*records out.*real.*user.*sys.*>" {
                case_pass $case_num
                break
            }
            timeout {
                send_user "Generate a 10M file on $mnt_point fail\n"
                case_fail $case_num
            }
        }
    }

    incr case_num
    set timeout 10
    send "rm $mnt_point/usbhost_testfile; rm $mnt_point/10m.bin\r"
    while 1 {
        expect {
            -re "rm:.*>" {
                send_user "Remove file on $mnt_point fail\n"
                case_fail $case_num
            }
            -re $kernel_prompt {
                break
            }
            timeout {
                send_user "Remove file on $mnt_point timeout\n"
                case_fail $case_num
            }
        }
    }

    send "ls $mnt_point/usbhost_testfile; ls $mnt_point/10m.bin\r"
    while 1 {
        expect {
            -re "No such file or directory.*No such file or directory.*>" {
                case_pass $case_num
                break
            }
            timeout {
                send_user "Remove file on $mnt_point fail\n"
                case_fail $case_num
            }
        }
    }

    incr case_num
    send "umount $mnt_point\r"
    while 1 {
        expect {
            -re "umount:.*>" {
                send_user "umount $mnt_point error\n"
                case_fail $case_num
            }
            -re $kernel_prompt {
                break
            }
            timeout {
                send -s "\3"
                send_user "umount $mnt_point timeout\n"
                break
            }
        }
    }

    send "mount\r"
    while 1 {
        expect {
            -re "$device on $mnt_point type vfat.*>" {
                send_user "umount $mnt_point fail"
                case_fail $case_num
            }
            -re $kernel_prompt {
                case_pass $case_num
                break
            }
            timeout {
                send_user "umount $mnt_point fail\n"
                case_fail $case_num
            }
        }
    }
}


step "Start testing"
sleep 10
send "mkdir -p /mnt/a; mkdir -p /mnt/b\r"
expect  -re $kernel_prompt

set case_num 0

sleep 5
incr case_num
set timeout 10
send "ls /dev/sd*\r"
while 1 {
    expect {
       -re "sda.*sda1.*sdb.*sdb1.*>" {
           case_pass $case_num
           break
       }
       timeout {
           case_fail $case_num
       }
    }
}

send_user "Start to test on /dev/sda1\n"
usb_test /dev/sda1 /mnt/a

send_user "Start to test /dev/sdb1\n"
usb_test /dev/sdb1 /mnt/b

all_pass

send_user "Ending $argv0\n"

log_file
