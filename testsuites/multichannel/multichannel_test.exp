#!/usr/bin/expect

source  ../kernel_config.exp
log_file [log_file_name "$argv0"]
send_user "Starting $argv0\n"
set TITLE [title "$argv0"]

if { $argc < 2} {
    puts "Please input: board_type, audio_dev"
    puts "audio_dev: audio_ad1836, audio_ad1938"
    exit
}
set audio_dev [lindex $argv 1]
append TITLE " ($audio_dev)"

step "Spawn kermit"
source ../spawn_kermit.exp

step "Reboot the kernel."
# To add following line here to indicate that if the bootargs is different,
# it could be set here. If the bootargs value is 0, it needn't be stated,
# for it is the default value.
# set bootargs  $bootargs_param0
source ../reboot_kernel.exp
sleep 15
send "\r"
expect -re $kernel_prompt

step "Start testing"
set case_num 0

if { $audio_dev == "audio_ad1836" } {
    set modprobe_cmd "modprobe snd_soc_ad1836; sleep 1; modprobe snd_soc_bf5xx_tdm; sleep 1; modprobe snd_ad1836"
    set channel_num 6
} elseif { $audio_dev == "audio_ad1938" } {
    set modprobe_cmd "modprobe snd_soc_ad193x; sleep 1; modprobe snd_soc_bf5xx_tdm; sleep 1; modprobe snd-ad193x"
    set channel_num 8
}

sleep 5
set timeout 30
incr case_num
send "$modprobe_cmd\r"
while 1 {
    expect {
        -re "(modprobe:|\[eE]rror|\[fF]ail).*$kernel_prompt" {
            case_fail $case_num
        }
        -re "mapping ok.*$kernel_prompt" {
            case_pass $case_num
            break
        }
        timeout {
            send -s "\3"
            send_log "timeout\n"
            case_fail $case_num
        }
    }
}

sleep 10

set count_under_run 0
set timeout 120
incr case_num
send "speaker-test -c $channel_num\r"
while 1 {
    expect {
        -re "TUV|UVF|TU|UV|VF" {
            incr count_under_run
            continue
        }
        -re "LFE.*LFE.*LFE.*Time per period" {
            send -s "\3"
            while 1 {
                expect {
                    -re $kernel_prompt {
                        case_pass $case_num
                        break
                    }
                    timeout {
                        send_log "fail to stop speaker-test\n"
                        case_fail $case_num
                    }
                }
            }
            break
        }
        timeout {
            send_log "timeout\n"
            case_fail $case_num
        }
    }
}

incr case_num
if { $count_under_run == 0 } {
    send_user "No under run error\n"
    case_pass $case_num
} else {
    send_user "Under run error occurs $count_under_run times\n"
    case_fail $case_num
}

all_pass
send_user "Ending $argv0\n"
log_file
