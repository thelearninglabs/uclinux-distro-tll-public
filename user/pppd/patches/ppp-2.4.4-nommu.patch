tdb.c:
	Do not let the child free up the memory regions used by the tdb.

main.c:
	The child may not return from its function, but it can call other
	functions.  So pass the function/arguments that the child needs
	to execute as arguments to the safe fork function.

--- ppp-2.4.4/pppd/tdb.c
+++ ppp-2.4.4/pppd/tdb.c
@@ -1866,6 +1866,9 @@ TDB_CONTEXT *tdb_open_ex(const char *nam
  **/
 int tdb_close(TDB_CONTEXT *tdb)
 {
+#ifdef NOMMU
+	return close(tdb->fd);
+#else
 	TDB_CONTEXT **i;
 	int ret = 0;
 
@@ -1892,6 +1895,7 @@ int tdb_close(TDB_CONTEXT *tdb)
 	SAFE_FREE(tdb);
 
 	return ret;
+#endif
 }
 
 /* lock/unlock entire database */
--- ppp-2.4.4/pppd/main.c
+++ ppp-2.4.4/pppd/main.c
@@ -1524,8 +1524,12 @@ bad_signal(sig)
  * This also arranges for the specified fds to be dup'd to
  * fds 0, 1, 2 in the child.
  */
+struct child_tail {
+	void (*followup)(void *args);
+	void *args;
+};
 pid_t
-safe_fork(int infd, int outfd, int errfd)
+safe_fork_tail(int infd, int outfd, int errfd, struct child_tail *tail)
 {
 	pid_t pid;
 	int fd, pipefd[2];
@@ -1541,7 +1545,7 @@ safe_fork(int infd, int outfd, int errfd
 
 	if (pipe(pipefd) == -1)
 		pipefd[0] = pipefd[1] = -1;
-#ifdef __uClinux__
+#ifdef NOMMU
 	pid = vfork();
 #else
 	pid = fork();
@@ -1602,14 +1606,36 @@ safe_fork(int infd, int outfd, int errfd
 	/* this close unblocks the read() call above in the parent */
 	close(pipefd[1]);
 
+	if (tail)
+		tail->followup(tail->args);
 	return 0;
 }
+pid_t
+safe_fork(int infd, int outfd, int errfd)
+{
+	return safe_fork_tail(infd, outfd, errfd, NULL);
+}
 
 /*
  * device_script - run a program to talk to the specified fds
  * (e.g. to run the connector or disconnector script).
  * stderr gets connected to the log fd or to the _PATH_CONNERRS file.
  */
+void device_script_tail(void *args)
+{
+    char *program = args;
+    /* here we are executing in the child */
+
+    setgid(getgid());
+    setuid(uid);
+    if (getuid() != uid) {
+	fprintf(stderr, "pppd: setuid failed\n");
+	_exit(1);
+    }
+    execl("/bin/sh", "sh", "-c", program, (char *)0);
+    perror("pppd: could not exec /bin/sh");
+    _exit(99);
+}
 int
 device_script(program, in, out, dont_wait)
     char *program;
@@ -1619,6 +1645,7 @@ device_script(program, in, out, dont_wai
     int pid;
     int status = -1;
     int errfd;
+    struct child_tail tail = { device_script_tail, program };
 
     if (log_to_fd >= 0)
 	errfd = log_to_fd;
@@ -1626,7 +1653,7 @@ device_script(program, in, out, dont_wai
 	errfd = open(_PATH_CONNERRS, O_WRONLY | O_APPEND | O_CREAT, 0600);
 
     ++conn_running;
-    pid = safe_fork(in, out, errfd);
+    pid = safe_fork_tail(in, out, errfd, &tail);
 
     if (pid != 0 && log_to_fd < 0)
 	close(errfd);
@@ -1652,17 +1679,7 @@ device_script(program, in, out, dont_wai
 	return (status == 0 ? 0 : -1);
     }
 
-    /* here we are executing in the child */
-
-    setgid(getgid());
-    setuid(uid);
-    if (getuid() != uid) {
-	fprintf(stderr, "pppd: setuid failed\n");
-	exit(1);
-    }
-    execl("/bin/sh", "sh", "-c", program, (char *)0);
-    perror("pppd: could not exec /bin/sh");
-    exit(99);
+    _exit(99);
     /* NOTREACHED */
 }
 
@@ -1677,6 +1694,42 @@ device_script(program, in, out, dont_wai
  * If done != NULL, (*done)(arg) will be called later (within
  * reap_kids) iff the return value is > 0.
  */
+struct run_program_args {
+    char *prog;
+    char **args;
+    int must_exist;
+};
+void
+run_program_tail(void *vargs)
+{
+    struct run_program_args *rpargs = vargs;
+    char *prog = rpargs->prog;
+    char **args = rpargs->args;
+    int must_exist = rpargs->must_exist;
+
+    /* Leave the current location */
+    (void) setsid();	/* No controlling tty. */
+    (void) umask (S_IRWXG|S_IRWXO);
+    (void) chdir ("/");	/* no current directory. */
+    setuid(0);		/* set real UID = root */
+    setgid(getegid());
+
+#ifdef BSD
+    /* Force the priority back to zero if pppd is running higher. */
+    if (setpriority (PRIO_PROCESS, 0, 0) < 0)
+	warn("can't reset priority to 0: %m");
+#endif
+
+    /* run the program */
+    execve(prog, args, script_env);
+    if (must_exist || errno != ENOENT) {
+	/* have to reopen the log, there's nowhere else
+	   for the message to go. */
+	reopen_log();
+	syslog(LOG_ERR, "Can't execute %s: %m", prog);
+	closelog();
+    }
+}
 pid_t
 run_program(prog, args, must_exist, done, arg, wait)
     char *prog;
@@ -1688,6 +1741,8 @@ run_program(prog, args, must_exist, done
 {
     int pid, status;
     struct stat sbuf;
+    struct run_program_args vargs = { prog, args, must_exist };
+    struct child_tail tail = { run_program_tail, &vargs };
 
     /*
      * First check if the file exists and is executable.
@@ -1703,7 +1758,7 @@ run_program(prog, args, must_exist, done
 	return 0;
     }
 
-    pid = safe_fork(fd_devnull, fd_devnull, fd_devnull);
+    pid = safe_fork_tail(fd_devnull, fd_devnull, fd_devnull, &tail);
     if (pid == -1) {
 	error("Failed to create child process for %s: %m", prog);
 	return -1;
@@ -1723,28 +1778,6 @@ run_program(prog, args, must_exist, done
 	return pid;
     }
 
-    /* Leave the current location */
-    (void) setsid();	/* No controlling tty. */
-    (void) umask (S_IRWXG|S_IRWXO);
-    (void) chdir ("/");	/* no current directory. */
-    setuid(0);		/* set real UID = root */
-    setgid(getegid());
-
-#ifdef BSD
-    /* Force the priority back to zero if pppd is running higher. */
-    if (setpriority (PRIO_PROCESS, 0, 0) < 0)
-	warn("can't reset priority to 0: %m");
-#endif
-
-    /* run the program */
-    execve(prog, args, script_env);
-    if (must_exist || errno != ENOENT) {
-	/* have to reopen the log, there's nowhere else
-	   for the message to go. */
-	reopen_log();
-	syslog(LOG_ERR, "Can't execute %s: %m", prog);
-	closelog();
-    }
     _exit(-1);
 }
 
