# Italian translations for cpufrequtils package
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the cpufrequtils package.
# Mattia Dongili <malattia@gmail.com>, 2004-2005.
#
#
msgid ""
msgstr ""
"Project-Id-Version: cpufrequtils 0.3\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-07-17 11:29+0200\n"
"PO-Revision-Date: 2005-05-04 20:00+0100\n"
"Last-Translator: Mattia Dongili <malattia@gmail.com>\n"
"Language-Team: NONE\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=ISO-8859-1\n"
"Content-Transfer-Encoding: 8bit\n"

#: utils/info.c:36
#, c-format
msgid "Couldn't count the number of CPUs (%s: %s), assuming 1\n"
msgstr "Impossibile determinare il numero di CPU (%s: %s), assumo 1\n"

#: utils/info.c:67
#, c-format
msgid ""
"          minimum CPU frequency  -  maximum CPU frequency  -  governor\n"
msgstr ""
"          frequenza minima CPU   -  frequenza massima CPU  -  gestore\n"

#: utils/info.c:128
#, c-format
msgid "couldn't analyze CPU %d as it doesn't seem to be present\n"
msgstr "impossibile analizzare la CPU %d poiche` non sembra essere presente\n"

#: utils/info.c:132
#, c-format
msgid "analyzing CPU %d:\n"
msgstr "analisi della CPU %d:\n"

#: utils/info.c:139
#, c-format
msgid "  no or unknown cpufreq driver is active on this CPU\n"
msgstr "  nessun modulo o modulo cpufreq sconosciuto per questa CPU\n"

#: utils/info.c:141
#, c-format
msgid "  driver: %s\n"
msgstr "  modulo %s\n"

#: utils/info.c:147
#, c-format
msgid "  CPUs which need to switch frequency at the same time: "
msgstr ""
"  CPU per le quali e` necessario cambiare la frequenza contemporaneamente: "

#: utils/info.c:157
#, c-format
msgid "  hardware limits: "
msgstr "  limiti hardware: "

#: utils/info.c:166
#, c-format
msgid "  available frequency steps: "
msgstr "  frequenze disponibili: "

#: utils/info.c:179
#, c-format
msgid "  available cpufreq governors: "
msgstr "  gestori disponibili: "

#: utils/info.c:190
#, c-format
msgid "  current policy: frequency should be within "
msgstr "  gestore corrente: la frequenza deve mantenersi tra "

#: utils/info.c:192
#, c-format
msgid " and "
msgstr " e "

#: utils/info.c:196
#, c-format
msgid ""
"The governor \"%s\" may decide which speed to use\n"
"                  within this range.\n"
msgstr ""
" Il gestore \"%s\" puo` decidere quale velocita` usare\n"
"                  in questo intervallo.\n"

#: utils/info.c:203
#, c-format
msgid "  current CPU frequency is "
msgstr "  la frequenza attuale della CPU e` "

#: utils/info.c:206
#, c-format
msgid " (asserted by call to hardware)"
msgstr " (ottenuta da una chiamata diretta all'hardware)"

#: utils/info.c:214
#, c-format
msgid "  cpufreq stats: "
msgstr " statistiche cpufreq:"

#: utils/info.c:361 utils/set.c:30
#, c-format
msgid "Report errors and bugs to %s, please.\n"
msgstr "Per favore, comunicare errori e malfunzionamenti a %s.\n"

#: utils/info.c:365
#, c-format
msgid "Usage: cpufreq-info [options]\n"
msgstr "Uso: cpufreq-info [opzioni]\n"

#: utils/info.c:366 utils/set.c:35
#, c-format
msgid "Options:\n"
msgstr "Opzioni:\n"

#: utils/info.c:367
#, c-format
msgid ""
"  -c CPU, --cpu CPU    CPU number which information shall be determined "
"about\n"
msgstr ""
"  -c CPU, --cpu CPU    Numero di CPU per la quale ottenere le informazioni\n"

#: utils/info.c:368
#, c-format
msgid "  -e, --debug          Prints out debug information\n"
msgstr "  -e, --debug          Mostra le informazioni di debug\n"

#: utils/info.c:369
#, c-format
msgid ""
"  -f, --freq           Get frequency the CPU currently runs at, according\n"
"                       to the cpufreq core *\n"
msgstr ""
"  -f, --freq           Mostra la frequenza attuale della CPU secondo\n"
"                       il modulo cpufreq *\n"

#: utils/info.c:371
#, c-format
msgid ""
"  -w, --hwfreq         Get frequency the CPU currently runs at, by reading\n"
"                       it from hardware (only available to root) *\n"
msgstr ""
"  -w, --hwfreq         Mostra la frequenza attuale della CPU leggendola\n"
"                       dall'hardware (disponibile solo per l'utente root) *\n"

#: utils/info.c:373
#, c-format
msgid ""
"  -l, --hwlimits       Determine the minimum and maximum CPU frequency "
"allowed *\n"
msgstr ""
"  -l, --hwlimits       Determina le frequenze minima e massima possibiliper "
"la CPU *\n"

#: utils/info.c:374
#, c-format
msgid "  -d, --driver         Determines the used cpufreq kernel driver *\n"
msgstr ""
"  -d, --driver         Determina il modulo cpufreq del kernel in uso *\n"

#: utils/info.c:375
#, c-format
msgid "  -p, --policy         Gets the currently used cpufreq policy *\n"
msgstr ""
"  -p, --policy         Mostra il gestore cpufreq attualmente in uso *\n"

#: utils/info.c:376
#, c-format
msgid "  -g, --governors      Determines available cpufreq governors *\n"
msgstr "  -g, --governors      Determina i gestori cpufreq disponibili *\n"

#: utils/info.c:377
#, c-format
msgid ""
"  -a, --affected-cpus  Determines which CPUs can only switch frequency at "
"the\n"
"                       same time *\n"
msgstr ""
"  -a, --affected-cpus  Determina quali CPU devono cambiare frequenza\n"
"                       contemporaneamente *\n"

#: utils/info.c:379
#, c-format
msgid "  -s, --stats          Shows cpufreq statistics if available\n"
msgstr "  -s, --stats          Mostra le statistiche se disponibili\n"

#: utils/info.c:380
#, c-format
msgid ""
"  -o, --proc           Prints out information like provided by the /proc/"
"cpufreq\n"
"                       interface in 2.4. and early 2.6. kernels\n"
msgstr ""
"  -o, --proc           Stampa le informazioni come se provenissero dalla\n"
"                       interfaccia cpufreq /proc/ presente nei kernel\n"
"                       2.4 ed i primi 2.6\n"

#: utils/info.c:382
#, c-format
msgid ""
"  -m, --human          human-readable output for the -f, -w and -s "
"parameters\n"
msgstr ""
"  -m, --human          formatta l'output delle opzioni -f, -w e -s in "
"maniera\n"
"                       leggibile da un essere umano\n"

#: utils/info.c:383 utils/set.c:42
#, c-format
msgid "  -h, --help           Prints out this screen\n"
msgstr "  -h, --help           Stampa questa schermata\n"

#: utils/info.c:386
#, c-format
msgid ""
"If no argument or only the -c, --cpu parameter is given, debug output about\n"
"cpufreq is printed which is useful e.g. for reporting bugs.\n"
msgstr ""
"Se non viene specificata nessuna opzione o viene specificata solo l'opzione -"
"c, --cpu,\n"
"le informazioni di debug per cpufreq saranno utili ad esempio a riportare i "
"bug.\n"

#: utils/info.c:388
#, c-format
msgid ""
"For the arguments marked with *, omitting the -c or --cpu argument is\n"
"equivalent to setting it to zero\n"
msgstr ""
"Per le opzioni segnalate con *, omettere l'opzione -c o --cpu e` come "
"specificarla\n"
"con il valore 0\n"

#: utils/info.c:478
#, c-format
msgid ""
"The argument passed to this tool can't be combined with passing a --cpu "
"argument\n"
msgstr ""
"L'opzione specificata a questo programma non puo` essere combinata con --"
"cpu\n"

#: utils/info.c:491
#, c-format
msgid ""
"You can't specify more than one --cpu parameter and/or\n"
"more than one output-specific argument\n"
msgstr ""
"Non e` possibile specificare piu` di una volta l'opzione --cpu e/o\n"
"specificare piu` di un parametro di output specifico\n"

#: utils/info.c:497 utils/set.c:79
#, c-format
msgid "invalid or unknown argument\n"
msgstr "opzione sconosciuta o non valida\n"

#: utils/set.c:34
#, c-format
msgid "Usage: cpufreq-set [options]\n"
msgstr "Uso: cpufreq-set [opzioni]\n"

#: utils/set.c:36
#, c-format
msgid ""
"  -c CPU, --cpu CPU        number of CPU where cpufreq settings shall be "
"modified\n"
msgstr ""
"  -c CPU, --cpu CPU        numero di CPU per la quale modificare le "
"impostazioni\n"

#: utils/set.c:37
#, c-format
msgid ""
"  -d FREQ, --min FREQ      new minimum CPU frequency the governor may "
"select\n"
msgstr ""
"  -d FREQ, --min FREQ      la nuova frequenza minima che il gestore cpufreq "
"puo` scegliere\n"

#: utils/set.c:38
#, c-format
msgid ""
"  -u FREQ, --max FREQ      new maximum CPU frequency the governor may "
"select\n"
msgstr ""
"  -u FREQ, --max FREQ      la nuova frequenza massima che il gestore cpufreq "
"puo` scegliere\n"

#: utils/set.c:39
#, c-format
msgid "  -g GOV, --governor GOV   new cpufreq governor\n"
msgstr "  -g GOV, --governor GOV   nuovo gestore cpufreq\n"

#: utils/set.c:40
#, c-format
msgid ""
"  -f FREQ, --freq FREQ     specific frequency to be set. Requires userspace\n"
"                           governor to be available and loaded\n"
msgstr ""
"  -f FREQ, --freq FREQ     specifica la frequenza a cui impostare la CPU.\n"
"                           E` necessario che il gestore userspace sia "
"disponibile e caricato\n"

#: utils/set.c:44
#, c-format
msgid ""
"Notes:\n"
"1. Omitting the -c or --cpu argument is equivalent to setting it to zero\n"
"2. The -f FREQ, --freq FREQ parameter cannot be combined with any other "
"parameter\n"
"   except the -c CPU, --cpu CPU parameter\n"
"3. FREQuencies can be passed in Hz, kHz (default), MHz, GHz, or THz\n"
"   by postfixing the value with the wanted unit name, without any space\n"
"   (FREQuency in kHz =^ Hz * 0.001 =^ MHz * 1000 =^ GHz * 1000000).\n"
msgstr ""
"Note:\n"
"1. Omettere l'opzione -c o --cpu e` equivalente a impostarlo a 0\n"
"2. l'opzione -f FREQ, --freq FREQ non puo` essere specificata con altre "
"opzioni\n"
"   eccetto l'opzione -c CPU, --cpu CPU\n"
"3. le FREQuenze possono essere specuficate in  Hz, kHz (default), MHz, GHz, "
"or THz\n"
"   postponendo l'unita` di misura al valore senza nessuno spazio fra loro\n"
"   (FREQuenza in kHz =^ Hz * 0.001 =^ MHz * 1000 =^ GHz * 1000000).\n"

#: utils/set.c:251
#, c-format
msgid ""
"the -f/--freq parameter cannot be combined with -d/--min, -u/--max or\n"
"-g/--governor parameters\n"
msgstr ""
"l'opzione -f/--freq non puo` venire combinata con i parametri\n"
" -d/--min, -u/--max o -g/--governor\n"

#: utils/set.c:262
#, c-format
msgid ""
"At least one parameter out of -f/--freq, -d/--min, -u/--max, and\n"
"-g/--governor must be passed\n"
msgstr ""
"Almeno una delle opzioni -f/--freq, -d/--min, -u/--max, e -g/--governor\n"
"deve essere specificata\n"

#: utils/set.c:282
#, c-format
msgid "wrong, unknown or unhandled CPU?\n"
msgstr "CPU errata, sconosciuta o non gestita?\n"

#: utils/set.c:306
#, c-format
msgid ""
"Error setting new values. Common errors:\n"
"- Do you have proper administration rights? (super-user?)\n"
"- Is the governor you requested available and modprobed?\n"
"- Trying to set an invalid policy?\n"
"- Trying to set a specific frequency, but userspace governor is not "
"available,\n"
"   for example because of hardware which cannot be set to a specific "
"frequency\n"
"   or because the userspace governor isn't loaded?\n"
msgstr ""
"Si sono verificati degli errori impostando i nuovi valori.\n"
"Alcuni errori comuni possono essere:\n"
"- Hai i necessari diritti di amministrazione? (super-user?)\n"
"- Il gestore che hai richiesto e` disponibile e caricato?\n"
"- Stai provando ad impostare una politica di gestione non valida?\n"
"- Stai provando a impostare una specifica frequenza ma il gestore\n"
"  userspace non e` disponibile, per esempio a causa dell'hardware\n"
"  che non supporta frequenze specifiche o a causa del fatto che\n"
"  il gestore userspace non e` caricato?\n"
