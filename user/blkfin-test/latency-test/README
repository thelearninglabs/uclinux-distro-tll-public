Test for measure IRQ latency and schedule latency.

It is copied and modified from the latency test case in Xenomai project.
By: yi.li@analog.com

Last update: 2007-12-24

Files:

latency.c: User application
timerbench.c: kernel module - it is basically a Watchdog timer driver
timerbench.h: header

Build:

Tested on 2008R1 kernel and toolchain. 
Selecting:
Blackfin test programs --->
   IRQ schedule latency test

Test:

After kernel boot on BF537 stamp

1. root:~> mknod /dev/timerbench0 c 222 0

2. root:~> insmod timerbench.ko

3. To test schedule latency, run the test case in test mode 0, and set
   period to be 20000 us.

   root:~> ./latency -t0 -p20000

   3.1 Here is sample output when the system is not busy:

<snip>
root:~> ./latency -t0 -p20000
== Sampling period: 20000 us
== Test mode: periodic user-mode task
== All results in microseconds
warming up...
RTT|  00:00:01  (periodic user-mode task, 20000 us period, priority 99)
RTH|-----lat min|-----lat avg|-----lat max|-overrun|----lat best|---lat worst
RTD|      23.000|      24.000|      30.000|       0|      23.000|      30.000
RTD|      23.000|      24.000|      40.000|       0|      23.000|      40.000
RTD|      23.000|      24.000|      36.000|       0|      23.000|      40.000
RTD|      24.000|      24.000|      37.000|       0|      23.000|      40.000
<snip>

   The test case will set the Watchdog Timer then wait for the Watchdog Timer interrupt
   to wake it up, and the kernel scheduler scheduels it to work again. In this example, 
   it does 50 (1 sec / 20000 us) tests per second.
   
   As we can see, the worst case schedule latency is about 40 us - not bad.

   3.2 However, if we start some workload in kernel, e.g.

   root:~> while [ 1 ]; do cat /proc/meminfo > /dev/null ; done

   Then we do the same test:

   root:~> ./latency -t0 -p20000

<snip>
root:~> ./latency -t0 -p20000
== Sampling period: 20000 us
== Test mode: periodic user-mode task
== All results in microseconds
warming up...
RTT|  00:00:01  (periodic user-mode task, 20000 us period, priority 99)
RTH|-----lat min|-----lat avg|-----lat max|-overrun|----lat best|---lat worst
RTD|      41.000|    2451.000|    9428.000|       0|      41.000|    9428.000
RTD|      44.000|    2346.000|    9016.000|       0|      41.000|    9428.000
RTD|      53.000|    2399.000|    8822.000|       0|      41.000|    9428.000
RTD|      45.000|    2314.000|    8482.000|       0|      41.000|    9428.000
RTD|      58.000|    2329.000|   10138.000|       0|      41.000|   10138.000
<snip>
   
   The worst case schedule latency becomes 10ms, even if we set the test case as real-time: SCHED_FIFO, Priority: 99.
   
   That is why we need patch like Adeos to make uClinux real-time.

4. Test interrupt latency:

   Run the test in test mode 2, taking 5000 ( 1 sec / 200 us) samples per second.

   root:~> latency -t2 -p200  

<NOTE> run this comand in a serial console - you will see very large interrupt latency:

<snip>
RTT|  00:00:01  (in-kernel timer handler, 200 us period, priority 99)
RTH|-----lat min|-----lat avg|-----lat max|-overrun|----lat best|---lat worst
RTD|       4.000|       4.133|       6.000|       0|       4.000|       6.000
RTD|       4.000|       4.153|      10.000|       0|       4.000|      10.000
RTD|       4.000|       4.781|     172.000|       0|       4.000|     172.000
RTD|       4.000|       4.873|     180.000|       0|       4.000|     180.000
RTD|       4.000|       4.792|     170.000|       0|       4.000|     180.000
RTD|       4.000|       4.911|     176.000|       0|       4.000|     180.000
RTD|       4.000|       4.778|     169.000|       0|       4.000|     180.000
<snip>

    If run this test in a telnet console, latency is much better:

<snip>
RTD|       3.000|       6.801|      41.000|       0|       3.000|      63.000
RTD|       3.000|       6.772|      40.000|       0|       3.000|      63.000
RTD|       3.000|       6.744|      40.000|       0|       3.000|      63.000
RTD|       3.000|       6.732|      40.000|       0|       3.000|      63.000
RTD|       3.000|       6.702|      38.000|       0|       3.000|      63.000
RTD|       3.000|       5.398|      37.000|       0|       3.000|      63.000
RTD|       4.000|       4.219|      27.000|       0|       3.000|      63.000
RTD|       4.000|       4.200|      26.000|       0|       3.000|      63.000
<snip>

   It looks some kernel code can block interrupt handling.

