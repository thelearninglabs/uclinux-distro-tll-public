# This is a pretty "full" device table that is meant to be used with
# a system that does not create device nodes on the fly.

# When building a target filesystem, it is desirable to not have to
# become root and then run 'mknod' a thousand times.  Using a device
# table you can create device nodes and directories "on the fly".
#
# This is a sample device table file for use with genext2fs.  You can
# do all sorts of interesting things with a device table file.  For
# example, if you want to adjust the permissions on a particular file
# you can just add an entry like:
#   /sbin/foobar        f       2755    0       0       -       -       -       -       -
# and (assuming the file /sbin/foobar exists) it will be made setuid
# root (regardless of what its permissions are on the host filesystem.
# Furthermore, you can use a single table entry to create a many device
# minors.  For example, if I wanted to create /dev/hda and /dev/hda[0-15]
# I could just use the following two table entries:
#   /dev/hda    b       640     0       0       3       0       0       0       -
#   /dev/hda    b       640     0       0       3       1       1       1       15
#
# Device table entries take the form of:
# <name>    <type>      <mode>  <uid>   <gid>   <major> <minor> <start> <inc>   <count>
# where name is the file name,  type can be one of:
#       f       A regular file
#       d       Directory
#       c       Character special device file
#       b       Block special device file
#       p       Fifo (named pipe)
# uid is the user id for the target file, gid is the group id for the
# target file.  The rest of the entries (major, minor, etc) apply only
# to device special files.

# Have fun
# -Erik Andersen <andersen@codepoet.org>
#

#<name>		<type>	<mode>	<uid>	<gid>	<major>	<minor>	<start>	<inc>	<count>
/dev		d	755	0	0	-	-	-	-	-
/dev/input	d	755	0	0	-	-	-	-	-
/dev/usb	d	755	0	0	-	-	-	-	-
/dev/mtd0	c	640	0	0	90	0	0	0	-
/dev/mtd1	c	640	0	0	90	2	0	0	-
/dev/mtd2	c	640	0	0	90	4	0	0	-
/dev/mtd3	c	640	0	0	90	6	0	0	-
/dev/mtdblock	b	640	0	0	31	0	0	1	3
/dev/fb0	c	600	0	0	29	0	0	0	-
/dev/vout0	c	600	0	0	81	192	0	0	-
/dev/mem	c	640	0	0	1	1	0	0	-
/dev/kmem	c	640	0	0	1	2	0	0	-
/dev/null	c	666	0	0	1	3	0	0	-
/dev/zero	c	666	0	0	1	5	0	0	-
/dev/random	c	666	0	0	1	8	0	0	-
/dev/urandom	c	666	0	0	1	9	0	0	-
/dev/tty	c	666	0	0	5	0	0	0	-
/dev/ptyp	c	666	0	0	2	0	0	1	1
/dev/pts	d	755	0	0	-	-	-	-	-
/dev/ttyp	c	666	0	0	3	0	0	1	1
/dev/tty	c	666	0	0	4	0	0	1	7
/dev/console	c	600	0	0	5	1	0	0	-
/dev/ram0	b	777	0	0	1	0	0	0	-
/dev/ram1	b	644	0	0	1	1	0	0	-
/dev/nfs	b	777	0	0	0	255	0	0	-
#/dev/ram	b	640	0	0	1	1	0	0	-
#/dev/ram	b	640	0	0	1	0	0	1	-
#/dev/loop	b	640	0	0	7	0	0	1	-
/dev/ptmx	c	666	0	0	5	2	0	0	-
/dev/ttyS 	c	755	0	0	4	64	0	1	1
/dev/ttyJ0	c	755	0	0	204	186	0	0	-
/dev/ttyUSB	c	664	0	0	188	0	0	1	1
/dev/psaux	c	640	0	0	10	1	0	0	-
/dev/rtc0	c	640	0	0	254	0	0	0	-
#/dev/dpmc	c	640	0	0	10	254	0	0	-
#/dev/ppp	c	660	0	0	108	0	0	0	-
#/dev/pppox	c	660	0	0	144	0	0	1	4
#/dev/can	c	666	0	0	91	0	0	1	1
#/dev/ircomm	c	666	0	0	161	0	0	1	2

# Adjust permissions on some normal files
#/etc/shadow	f	600	0	0	-	-	-	-	-
#/bin/tinylogin	f	4755	0	0	-	-	-	-	-

# Sound devices (OSS)
/dev/dsp	c	664	0	0	14	3	0	0	-
/dev/mixer	c	664	0	0	14	0	0	0	-
# Sound devices (ALSA)
/dev/snd	d	755	0	0	-	-	-	-	-
/dev/snd/controlC0	c	664	0	0	116	0	0	0	-
/dev/snd/pcmC0D0c	c	664	0	0	116	24	0	0	-
/dev/snd/pcmC0D0p	c	664	0	0	116	16	0	0	-
/dev/snd/timer	c	664	0	0	116	33	0	0	-

# User-mode Linux stuff
#/dev/ubda	b	640	0	0	98	0	0	0	-
#/dev/ubda	b	640	0	0	98	1	1	1	15
/dev/i2c-	c	664	0	0	89	0	0	1	1
/dev/spi	c	664	0	0	252	0	0	0	-
/dev/input/event c	664	0	0	13	64	0	1	1
/dev/usb/mouse	c	664	0	0	180	16	0	1	1
/dev/ppi	c	664	0	0	241	0	0	1	1
/dev/time	c	664	0	0	10	101	0	0	-
#/dev/lcd	c	664	0	0	60	0	0	0	-
#/dev/ad9960	c	664	0	0	240	0	0	0	-
/dev/watchdog	c	664	0	0	10	130	0	0	-

# SCSI Devices
/dev/sda	b	640	0	0	8	0	0	0	-
/dev/sda	b	640	0	0	8	1	1	1	15
#/dev/sdb	b	640	0	0	8	16	0	0	-
#/dev/sdb	b	640	0	0	8	17	1	1	15
#/dev/sdc	b	640	0	0	8	32	0	0	-
#/dev/sdc	b	640	0	0	8	33	1	1	15
#/dev/sdd	b	640	0	0	8	48	0	0	-
#/dev/sdd	b	640	0	0	8	49	1	1	15
#/dev/sde	b	640	0	0	8	64	0	0	-
#/dev/sde	b	640	0	0	8	65	1	1	15
#/dev/sdf	b	640	0	0	8	80	0	0	-
#/dev/sdf	b	640	0	0	8	81	1	1	15
#/dev/sdg	b	640	0	0	8	96	0	0	-
#/dev/sdg	b	640	0	0	8	97	1	1	15
#/dev/sdh	b	640	0	0	8	112	0	0	-
#/dev/sdh	b	640	0	0	8	113	1	1	15
/dev/sg		c	640	0	0	21	0	0	1	1
#/dev/scd	b	640	0	0	11	0	0	1	15
#/dev/st	c	640	0	0	9	0	0	1	8
#/dev/nst	c	640	0	0	9	128	0	1	8
#/dev/st	c	640	0	0	9	32	1	1	4
#/dev/st	c	640	0	0	9	64	1	1	4
#/dev/st	c	640	0	0	9	96	1	1	4

# mmc block devices
/dev/mmcblk0	b	640	0	0	179	0	0	0	-
/dev/mmcblk0p	b	640	0	0	179	1	1	1	3
