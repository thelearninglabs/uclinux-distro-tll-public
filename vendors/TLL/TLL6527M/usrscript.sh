# @file usrscript.sh
# 
# @brief
# This is the user script which is saved in /mnr/usrflash. It does the following
# 1) Switches on the capture for sound
# 2) Sets the mic as input device.
#    Note: There is a problem with the current driver and during startup
#    the sound card has to be accessed for properly switching the capture source
#    So the sound card is accessed using arecord and aplay (as a hack) and then
#    the proper source of capture is set (as mic)
# 3) If the display is not loaded the bfin-lq035q1-fb module is loaded else
#    the gpio for enabling the backlight is switched on(this is a temporary
#    solution for the problem with the error caused due to backlight integration
#    with the driver)
# 4) If SD card is present then mount it to /mnt/sdcard
#    And if impresario is present in /mnt/usrflash then run Impresario
# 
# @version 1.2
# 
# @date 20121010
# 
# @author Karthikeyan GA
# 
# XqJv Copyright(c) 2010-2012 The Learning Labs,Inc. All Rights Reserved  jVkB
# 
# Organization: The Learning Labs
# 
# Tested: 
# Target TLL6527M V1.2
# 
# Revised: 20121010, Author: Karthikeyan GA, Notes: Added logic for
# switching on the backlight while modprobing the lcd driver. This is
# a fix for the bug with automatic backlight integration with lcd 
# driver
# 
# Created: 20120711, Author: Karthikeyan GA, Notes: Created base version

#Switching the audio capture on 
amixer cset name='Capture Switch' on
#Changing the source to Line In
amixer sset 'Input Mux' 'Mic'
#Hack for properly setting the input source
arecord -d 2 | aplay -d 2 
amixer sset 'Input Mux' 'Mic'
#Either load display driver or switch on the backlight
if [ ! -e /dev/fb0 ]
then
    modprobe bfin-lq035q1-fb
    #Switching on the backlight
    #Remove the following 3 lines if the backlight
    #is integrated with the driver itself
    echo 80 > /sys/class/gpio/export
    echo out > /sys/class/gpio/gpio80/direction
    echo 1 > /sys/class/gpio/gpio80/value
else
    #Switching on the backlight
    #Remove the following 3 lines if the backlight
    #is integrated with the lcd driver itself
    echo 80 > /sys/class/gpio/export
    echo out > /sys/class/gpio/gpio80/direction
    echo 1 > /sys/class/gpio/gpio80/value
fi
#If the sdcard is present mount it to /mnt/sdcard
if [ -e /dev/mmcblk0p1 ]
then
    mkdir /mnt/sdcard
    mount -t vfat /dev/mmcblk0p1 /mnt/sdcard
    #If impresario is present open it as a separate thread
    if [ -e /mnt/usrflash/Impresario ]
    then
        #Redirecting all stdio messages to lcd (fbcon)
        echo "Starting Impresario" > /dev/tty0
        /mnt/usrflash/Impresario >/dev/tty0  &
    fi
fi
#Code for enabling IIO framework with ad7495 driver
#Loading the IIO framework
#modprobe industrialio
#modprobe ring_sw
#modprobe ad7476
#modprobe iio-trig-periodic-rtc
#modprobe iio-trig-gpio
#modprobe iio-trig-sysfs
#modprobe iio-trig-bfin-timer
#Loading the TLL6527M PH9 device switch helper module
#modprobe tll6527m_sysfs_spi
#Load the ad7495 device
#echo ad7495 > /sys/firmware/tll6527m/spi/ph9device
#Fix: Sometimes the /dev/device0:buffer0 does not get loaded automatically
#echo spi0.49 > /sys/bus/spi/drivers/ad7476/bind
#samplingrate=100
#echo spi0.49 > /sys/bus/spi/
#echo $samplingrate > /sys/bus/iio/devices/trigger0/frequency
#echo bfintmr3 > /sys/bus/iio/devices/device0/trigger/current_trigger
#echo 5000 > /sys/bus/iio/devices/device0/device0:buffer0/length
#echo 0 > /sys/bus/iio/devices/device0/device0:buffer0/scan_elements/timestamp_en
#echo 1 > /sys/bus/iio/devices/device0/device0:buffer0/scan_elements/in0_en
#echo 1 > /sys/bus/iio/devices/device0/device0:buffer0/enable
